
<?php get_header(); ?>

<div class="container text-justify">



            <?php if(have_posts()) : ?>
                <?php while(have_posts()) : the_post(); ?>

                    <?php
                    $home=do_shortcode(get_post_key('_nimp_project_home_key'));
                    $start_date=do_shortcode(get_post_key('_nimp_project_start_date_key'));
                    $end_date=do_shortcode(get_post_key('_nimp_project_end_date_key'));
                    $components=do_shortcode(get_post_key('_nimp_project_components_key'));
                    $teams=do_shortcode(get_post_key('_nimp_project_teams_key'));
                    $news=do_shortcode(get_post_key('_nimp_project_news_key'));
                    $results=do_shortcode(get_post_key('_nimp_project_results_key'));
                    $activities=do_shortcode(get_post_key('_nimp_project_didactic_activities_key'));

                    $publications=do_shortcode(get_post_key('_nimp_project_publications_key'));
                    $miscellanous=do_shortcode(get_post_key('_nimp_project_miscellanous_key'));
                    $services=do_shortcode(get_post_key('_nimp_project_services_key'));
                    $contact=do_shortcode(get_post_key('_nimp_project_contact_key'));
                    ?>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 blog-main">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><h3><?php the_title(); ?></h3></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                                <?php if(isset($home)):?>
                                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab " aria-controls="v-pills-home" aria-selected="true">Home</a>
                                <?php endif;?>
                                <?php if(isset($components)):?>
                                    <a class="nav-link" id="v-pills-components-tab" data-toggle="pill" href="#v-pills-components" role="tab " aria-controls="v-pills-components" >Components</a>
                                <?php endif;?>
                                <?php if(isset($teams)):?>
                                    <a class="nav-link" id="v-pills-teams-tab" data-toggle="pill" href="#v-pills-teams" role="tab " aria-controls="v-pills-teams" >Project Teams</a>
                                <?php endif;?>
                                <?php if(isset($news)):?>
                                    <a class="nav-link" id="v-pills-news-tab" data-toggle="pill" href="#v-pills-news" role="tab " aria-controls="v-pills-news" >News</a>
                                <?php endif;?>
                                <?php if(isset($results)):?>
                                    <a class="nav-link" id="v-pills-results-tab" data-toggle="pill" href="#v-pills-results" role="tab " aria-controls="v-pills-results" >Scientific Results</a>
                                <?php endif;?>
                                <?php if(isset($activities)):?>
                                    <a class="nav-link" id="v-pills-activities-tab" data-toggle="pill" href="#v-pills-activities" role="tab " aria-controls="v-pills-activities" >Didactic Activities</a>
                                <?php endif;?>
                                <?php if(isset($publications)):?>
                                    <a class="nav-link" id="v-pills-publications-tab" data-toggle="pill" href="#v-pills-publications" role="tab " aria-controls="v-pills-publications" >Publications</a>
                                <?php endif;?>
                                <?php if(isset($miscellanous)):?>
                                    <a class="nav-link" id="v-pills-miscellanous-tab" data-toggle="pill" href="#v-pills-miscellanous" role="tab " aria-controls="v-pills-miscellanous" >Miscellanous</a>
                                <?php endif;?>
                                <?php if(isset($services)):?>
                                    <a class="nav-link" id="v-pills-services-tab" data-toggle="pill" href="#v-pills-services" role="tab " aria-controls="v-pills-services" >Services</a>
                                <?php endif;?>
                                <?php if(isset($contact)):?>
                                    <a class="nav-link" id="v-pills-contact-tab" data-toggle="pill" href="#v-pills-contact" role="tab " aria-controls="v-pills-contact" >Contact</a>
                                <?php endif;?>
                            </div>
                        </div>

                        <div class="col-lg-10 col-md-10 col-sm-12">
                            <div class="tab-content" id="v-pills-tabContent">

                                <?php if(isset($home)):?>
                                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <?php echo $home; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($components)):?>
                                    <div class="tab-pane fade show " id="v-pills-components" role="tabpanel" aria-labelledby="v-pills-components-tab">
                                        <?php echo $components; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($teams)):?>
                                    <div class="tab-pane fade show " id="v-pills-teams" role="tabpanel" aria-labelledby="v-pills-teams-tab">
                                        <?php echo $teams; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($news)):?>
                                    <div class="tab-pane fade show " id="v-pills-news" role="tabpanel" aria-labelledby="v-pills-news-tab">
                                        <?php echo $news; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($results)):?>
                                    <div class="tab-pane fade show " id="v-pills-results" role="tabpanel" aria-labelledby="v-pills-results-tab">
                                        <?php echo $results; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($activities)):?>
                                    <div class="tab-pane fade show " id="v-pills-activities" role="tabpanel" aria-labelledby="v-pills-activities-tab">
                                        <?php echo $activities; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($publications)):?>
                                    <div class="tab-pane fade show " id="v-pills-publications" role="tabpanel" aria-labelledby="v-pills-publications-tab">
                                        <?php echo $publications; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($miscellanous)):?>
                                    <div class="tab-pane fade show " id="v-pills-miscellanous" role="tabpanel" aria-labelledby="v-pills-miscellanous-tab">
                                        <?php echo $miscellanous; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($services)):?>
                                    <div class="tab-pane fade show " id="v-pills-services" role="tabpanel" aria-labelledby="v-pills-services-tab">
                                        <?php echo $services; ?>
                                    </div>
                                <?php endif;?>

                                <?php if(isset($contact)):?>
                                    <div class="tab-pane fade show " id="v-pills-contact" role="tabpanel" aria-labelledby="v-pills-contact-tab">
                                        <?php echo $contact; ?>
                                    </div>
                                <?php endif;?>

                            </div>
                        </div>
                    </div>



                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Posts Found'); ?></p>
            <?php endif; ?>



    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

</div><!-- /.container -->



<?php get_footer();?>

















































<?php

//Register Nav Walker class

require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';


//Theme suport

function nimp_theme_setup()
{

    add_theme_support('post-thumbnails');
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'NIMP PRIMARY MENU' ),
    ) );

    //Posts Format
    add_theme_support('post-formats',['aside','gallery']);
}


add_action('after_setup_theme','nimp_theme_setup');

//Excerpt Length Control

function set_excerpt_length()
{
    return 30;
}

function get_post_key($key)
{
    $content= get_post_meta(get_the_ID(),$key,true);

    if(strlen($content)<3){
        return null;
    }

   // $content = htmlspecialchars_decode($content);
    $content = wpautop( $content );
   return $content;
}

function posts_for_current_author($query) {
    global $pagenow;

    if( 'edit.php' != $pagenow || !$query->is_admin )
        return $query;

    if( !current_user_can( 'edit_others_posts' ) ) {
        global $user_ID;
        $query->set('author', $user_ID );
    }
    return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

add_filter('excerpt_length','set_excerpt_length');
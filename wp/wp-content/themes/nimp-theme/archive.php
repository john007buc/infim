
<?php get_header(); $i=1;?>

<div class="container text-justify">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 blog-main">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Scientific Projects</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 blog-main">

            <?php if(have_posts()) : ?>
                <?php while(have_posts()) : the_post(); ?>

                    <div class="card m-3 p-3">
                        <div class="card-title">
                            <h3>
                            <?php echo $i.'. '.get_the_title();$i++; ?>
                            </h3>
                        </div>
                        <div class="card-subtitle">
                            <i class="fa fa-user" aria-hidden="true"></i> Project Director:
                            <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
                                <?php echo get_the_author(); ?>
                            </a>,
                            Project Type: <?php echo get_post_meta(get_the_ID(),'_nimp_project_type_key',true); ?>,
                            <i class="fa fa-calendar" aria-hidden="true"></i> Start Date: <?php echo get_post_meta(get_the_ID(),'_nimp_project_start_date_key',true); ?>
                            <i class="fa fa-calendar" aria-hidden="true"></i> End Date: <?php echo get_post_meta(get_the_ID(),'_nimp_project_end_date_key',true); ?>
                        </div>
                        <div class="card-link">
                            <?php
                            $external_link=get_post_meta(get_the_ID(),'_nimp_project_external_link_key',true);
                            $link=(parse_url($external_link,PHP_URL_HOST))?$external_link:get_permalink();

                            //the_permalink()
                            ?>
                           <a class="btn btn-outline-secondary" href="<?php echo $link; ?>">View more</a>
                        </div>
                    </div>


                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Posts Found'); ?></p>
            <?php endif; ?>
        </div><!-- /.blog-main -->
    </div>

    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

</div><!-- /.container -->



<?php get_footer();?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php bloginfo('description'); ?>">

    <link rel="icon" href="">

    <title>
        <?php bloginfo('name'); ?>|
        <?php is_front_page()?bloginfo('description'):wp_title();?>
    </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url') ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this theme -->
    <link href="<?php bloginfo('stylesheet_url') ?>" rel="stylesheet">
    <!-- Custom font awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <?php wp_head();?>
</head>
<body>

<header>
    <nav class="navbar navbar-expand-md  my-navbar fixed-top bg-none1 py-md-2" id="navbarCollapse">

            <a  class="logo" href="/">
                <img  class="nimp-logo img-responsive" src="<?php bloginfo('template_url') ?>/img/logo.png" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="fa fa-navicon"></i></span>
            </button>

            <?php
            wp_nav_menu( array(
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'navbarCollapse',
                'menu_class'        => 'nav  ml-auto navbar-nav',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ) );
            ?>
    </nav>
</header>

<main role="main">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide img-responsive" src="<?php bloginfo('template_url') ?>/img/header1.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption text-center">
                        <h1><?php bloginfo('name'); ?></h1>
                        <p><?php bloginfo('description');?> </p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide img-responsive" src="<?php bloginfo('template_url') ?>/img/header1.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption">
                        <h1><?php bloginfo('name'); ?></h1>
                        <p><?php bloginfo('description');?> </p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide img-responsive" src="<?php bloginfo('template_url') ?>/img/header1.jpg" alt="Third slide">
                <div class="container">
                    <div class="carousel-caption text-center">
                        <h1><?php bloginfo('name'); ?></h1>
                        <p><?php bloginfo('description');?> </p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
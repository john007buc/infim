<!-- FOOTER -->
<footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; <?php echo Date('Y'); ?> <?php bloginfo('name'); ?></p>
</footer>
</main>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php bloginfo('template_url') ?>/js/jquery-3-3-1.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.js"></script>
<?php wp_footer();?>
</body>
</html>

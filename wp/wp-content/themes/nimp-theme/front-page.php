
<?php get_header(); ?>

<div class="container text-justify">
    <div class="row">
        <div class="col-sm-12  col-md-12 col-lg-12 blog-main">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
            </ol>
        </nav>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12  blog-main">

            <?php if(have_posts()) : ?>
                <?php while(have_posts()) : the_post(); ?>
                    <?php

                   the_content();
                  ?>
                <?php endwhile; ?>
            <?php else : ?>
                <p><?php __('No Posts Found'); ?></p>
            <?php endif; ?>
        </div><!-- /.blog-main -->
    </div>

    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

</div><!-- /.container -->



<?php get_footer();?>
<?php get_header(); ?>

    <div class="container text-justify">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 blog-main">
                <?php if(have_posts()) : ?>
                    <?php while(have_posts()) : the_post(); ?>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></li>
                            </ol>
                        </nav>

                    <?php the_content();endwhile; ?>
                <?php else : ?>
                    <p><?php __('No Page Found'); ?></p>
                <?php endif; ?>
            </div><!-- /.blog-main -->
        </div>

        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

    </div><!-- /.container -->



<?php get_footer();?>
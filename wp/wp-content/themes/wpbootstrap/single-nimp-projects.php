<?php
/**
 * The template for displaying all single posts and attachments
 *
 * The expected workflow should be:
 *  - get_template_part('template-parts/content', get_post_format());
 *      - i.e. template-parts/content-single.php
 *          - the content-files should act as a controler
 *          - from there the specific title-, meta-, content-functions or other are called
 *          - these functions are located in template-tags.php
 *
 * @package WordPress
 * @subpackage quicksand
 */
get_header();
?>

    <!--template: single-->
    <div class="row">

        <?php //quicksand_get_sidebars('left') ?>


        <!--  site-content-area -->
        <main id="primary" class="site-content-area">

            <!-- post-list -->
            <?php
            while (have_posts()) : the_post();
//            get_template_part('template-parts/content', 'single');
             //get_post_custom_keys()
                /*add_action('save_post',array($this->metaCallbacks,'saveProjectHomeCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectComponentsCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectTeamsCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectResultsCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectDidacticActivitiesCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectPublicationsCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectMiscellanousCallback'));
                add_action('save_post',array($this->metaCallbacks,'saveProjectContactCallback'));*/

                $title=get_the_title();
                echo "<h1>$title</h1>";





               echo "</br>";
                $content= get_post_meta(get_the_ID(),'_nimp_project_home_key',true);

                $content = htmlspecialchars_decode($content);
                $content = wpautop( $content );
                echo $content;


              // echo get_post_meta(get_the_ID(),'title',true);

                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) {
                    comments_template();
                }


            endwhile;
            ?>

            <div class="row">
                <div class="col-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</a>
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
                        <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</a>
                    </div>
                </div>
                <div class="col-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                            is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker i
                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">...</div>
                        <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
                        <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
                    </div>
                </div>
            </div>




            <!--posts-navigation-->
            <div class="post-pagination">
                <?php
                if (is_singular('attachment')) {
                    // Parent post navigation.
                    the_post_navigation(array(
                        'prev_text' => _x('<span class="meta-nav btn btn-outline-secondary">Published in </span><span class="post-title">%title</span>', 'Parent post link', 'quicksand'),
                    ));
                } elseif (is_singular('post')) {
                    // Previous/next post navigation.
                    the_post_navigation(array(
                        'prev_text' => '<span class="meta-nav post-last-link" aria-hidden="true"><i class="fa fa-long-arrow-left"></i>' . esc_html(__("Previous Post", "quicksand")) . '</span>' .
                            '<span class="screen-reader-text">' . __('Previous post:', 'quicksand') . '</span> ',
                        'next_text' => '<span class="meta-nav post-next-link " aria-hidden="true">' . esc_html(__("Next Post", "quicksand")) . '<i class="fa fa-long-arrow-right"></i></span>' .
                            '<span class="screen-reader-text">' . __('Next post:', 'quicksand') . '</span> ',
                    ));
                }
                ?>
            </div>

        </main><!-- .site-content-area  -->

        <?php //quicksand_get_sidebars('right') ?>

    </div><!-- row-->

<?php get_footer(); ?>
<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 11.11.2018
 * Time: 21:52
 */
namespace Callbacks;

class MetaBoxesCallbacks
{

   public function addEditordCallback($post,$editorId,$media=true)
   {

       list($nonceAction,$nonceField,$metaKey)=$this->formatMetaKeys($editorId);


       wp_nonce_field( $nonceAction,$nonceField);

       $value=get_post_meta($post->ID,$metaKey,true);


       wp_editor( $value, $editorId, array("media_buttons" => $media) );

   }

   public function saveEditorCallback($postID,$editorId)
   {

       list($nonceAction,$nonceField,$metaKey)=$this->formatMetaKeys($editorId);

       if(!isset($_POST[$nonceField])){
           return;
       }

       if(!wp_verify_nonce($_POST[$nonceField],$nonceAction)){
           return;
       }

       if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
           // return;
       }

       if(!current_user_can('edit_post',$postID)){
           return;
       }

       if(!isset($_POST[$editorId])){
           return;
       }

       if (!empty($_POST[$editorId])) {

           //$html=preg_replace('#<script(.*?)>(.*?)</script>#is', '', htmlspecialchars_decode($_POST[$editorId]));

           update_post_meta($postID,  $metaKey, wp_kses_post(balanceTags($_POST[$editorId],true)) );
       }

   }

   public function formatMetaKeys($name)
   {
       return [
           "{$name}_nonce_action",
           "{$name}_nonce_field",
           "_{$name}_key"
       ];
   }

   public function addInputType($post,$name,$type,$label=null)
   {
       list($nonceAction,$nonceField,$metaKey)=$this->formatMetaKeys($name);

       wp_nonce_field( $nonceAction,$nonceField);

       $value=get_post_meta($post->ID,$metaKey,true);

       if(isset($label)){
           echo "<label for='{$name}'>{$label}</label>";
       }
       echo "<input type='{$type}' id='{$name}' name='{$name}' value='{$value}'>";
   }

   public function saveInputType($postID,$name)
   {
       list($nonceAction,$nonceField,$metaKey)=$this->formatMetaKeys($name);

       if(!isset($_POST[$nonceField])){
           return;
       }

       if(!wp_verify_nonce($_POST[$nonceField],$nonceAction)){
           return;
       }

       if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
           // return;
       }

       if(!current_user_can('edit_post',$postID)){
           return;
       }

       if(!isset($_POST[$name])){
           return;
       }

       if (!empty($_POST[$name]) && strlen($_POST[$name])<300) {

           update_post_meta($postID,  $metaKey, sanitize_text_field($_POST[$name]));
       }
   }


    public function addProjectHomeCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_home');

    }

    public function saveProjectHomeCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_home');

    }

    public function addProjectComponentsCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_components',false);

    }
    public function saveProjectComponentsCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_components');

    }

    public function addProjectNewsCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_news');

    }

    public function saveProjectNewsCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_news');

    }


   public function addProjectTeamsCallback($post)
   {

       $this->addEditordCallback($post,'nimp_project_teams',false);

   }

   public function saveProjectTeamsCallback($postID)
   {
    $this->saveEditorCallback($postID,'nimp_project_teams');

   }

    public function addProjectResultsCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_results');
    }
    public function saveProjectResultsCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_results');
    }

    public function addProjectDidacticActivitiesCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_didactic_activities');

    }
    public function saveProjectDidacticActivitiesCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_didactic_activities');

    }

    public function addProjectPublicationsCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_publications');

    }
    public function saveProjectPublicationsCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_publications');

    }

    public function addProjectMiscellanousCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_miscellanous');

    }
    public function saveProjectMiscellanousCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_miscellanous');

    }

    public function addProjectServicesCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_services');

    }
    public function saveProjectServicesCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_services');

    }

    public function addProjectContactCallback($post)
    {
        $this->addEditordCallback($post,'nimp_project_contact',false);

    }
    public function saveProjectContactCallback($postID)
    {
        $this->saveEditorCallback($postID,'nimp_project_contact');

    }

    public function addProjectStartDateCallback($post)
    {
        $this->addInputType($post,'nimp_project_start_date','date');

    }


    public function saveProjectStartDateCallback($postID)
    {
        $this->saveInputType($postID,'nimp_project_start_date');

    }

    public function addProjectEndDateCallback($post)
    {
        $this->addInputType($post,'nimp_project_end_date','date');

    }


    public function saveProjectEndDateCallback($postID)
    {
        $this->saveInputType($postID,'nimp_project_end_date');

    }


    public function addProjectTypeCallback($post)
    {
        $this->addInputType($post,'nimp_project_type','text','Ex: PED,TE,PD,...');

    }


    public function saveProjectTypeCallback($postID)
    {
        $this->saveInputType($postID,'nimp_project_type');

    }

    public function addProjectExternalLinkCallback($post)
    {
        $this->addInputType($post,'nimp_project_external_link','text','Optional');

    }


    public function saveProjectExternalLinkCallback($postID)
    {
        $this->saveInputType($postID,'nimp_project_external_link');

    }

    public function addProjectRestrictedLinkCallback($post)
    {
        $this->addInputType($post,'nimp_project_restricted_link','text','Optional');

    }


    public function saveProjectRestrictedLinkCallback($postID)
    {
        $this->saveInputType($postID,'nimp_project_restricted_link');

    }






}

//https://stackoverflow.com/questions/3493313/how-to-add-wysiwyg-editor-in-wordpress-meta-box
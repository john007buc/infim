<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 13:47
 */

namespace Actions;

class Deactivate
{
    public static function deactivate()
    {
        flush_rewrite_rules();
    }
}
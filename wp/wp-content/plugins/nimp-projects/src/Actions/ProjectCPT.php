<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 15:38
 */
namespace Actions;

use Base\IService;

class ProjectCPT implements IService
{

    public function register()
    {
        add_action('init',array($this,'registerCPT'));
    }

    public function registerCPT()
    {
        register_post_type('nimp-projects',[
            'labels'=>[
                'name'=>'Scientific Projects',
                'singular_name'=>'Scientific Project',
                'add_new'=>'Add new project',
                'add_new_item'=>'Add new project',
                'edit_item'=>'Edit project',
                'new_item'=>'New project',
                'all_items'=>'All scientific projects',
                'view_item'=>'View project',
                'search_items'=>'Search projects',
                'menu_name'=>'Scientific Projects'
            ],
            'public'=>true,'has_archive'=>true,'rewrite'=>array('slug' => 'project'),'capability_type'=>'nimp-project','map_meta_cap'=>true,'hierarchical'=>false,'publicly_queryable' => true,
            'supports'=>['title','revisions','custom-fields','author','posts-format'],
            //'taxonomies'=>array('category'),
            'menu_icon'   => 'dashicons-category',
            'menu_positions'=>5
        ]);
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 14:34
 */

namespace Actions;

use Actions\Enqueue;
use Actions\ProjectCPT;
use Base\IService;
use Actions\Taxonomies;


final class Init
{

    /**
     * Store all the clases inside an array
     * @return array
     */
    public static function getServices()
    {
        return [
           Enqueue::class,
           ProjectCPT::class,
           Metaboxes::class,
           Taxonomies::class
        ];
    }

    /**
     * Loop through the classes, initialize them,
     * and call the register method if it exists
     */

    public static function  registerServices()
    {

        foreach(self::getServices() as $class){

            $service=self::instantiate($class);
            if($service instanceof IService){
                $service->register();
            }

        }
    }

    /**
     * Initialize the class
     * @param string $class class from the service array
     * @return Object new stance of the class
     */

    private static function instantiate($class)
    {
        return new $class();
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 12.11.2018
 * Time: 12:05
 */
namespace Actions;

use Base\IService;

class Taxonomies implements IService
{

    public function register()
    {
        add_action('init',[$this,'addCustomTaxonomies']);
    }

    public function addCustomTaxonomies()
    {

        register_taxonomy('project-type',['nimp-projects'],$this->getArgs());
    }


    public function getLabels()
    {
      return [
          'name'=>'Project Categories',
          'singular_name'=>'Project Category',
          'search_items'=>'Search Project Categories',
          'all_items'=>'All Project Categories',
          'parent_item'=>'Parent Project Category',
          'parent_item_colon'=>'Parent Project Category:',
          'edit_item'=>'Edit Project Category',
          'update_item'=>'Update Project Category',
          'add_new_item'=>'Add New Project Category',
          'new_item_name'=>'New Project Category',
          'menu_name'=>'Project Categories'
      ];
    }

    public function getArgs()
    {
        return [
            'hierarchical'=>true,
            'labels'=>$this->getLabels(),
            'show_ui'=>true,
            'show_admin_column'=>true,
            'query_var'=>true,
            'rewrite'=>['slug'=>'project-category']
        ];
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 13:24
 */
namespace Actions;


class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();
    }
}
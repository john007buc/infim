<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 15:21
 */

namespace Actions;


use Base\BaseController;

use Base\IService;

class Enqueue extends BaseController implements IService
{

   public function register()
   {
       add_action('admin_enqueue_scripts',array($this,'adminEnqueue'));
   }

    function adminEnqueue(){
        //wp_enqueue_style('mypluginstyle',$this->pluginURL.'assets/css/mystyle.css');
        //wp_enqueue_script('mypluginscript',$this->pluginURL.'assets/js/myscript.js');
    }

}
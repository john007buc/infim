<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 11.11.2018
 * Time: 21:51
 */

namespace Actions;

use Base\IService;

use Callbacks\MetaBoxesCallbacks;

class Metaboxes implements IService{


    public $metaCallbacks;
    public function __construct()
    {
        $this->metaCallbacks = new MetaBoxesCallbacks();
    }

    public function register()
    {
        add_action( 'add_meta_boxes', array($this,'addProjectMetaboxes') );

        add_action('save_post',array($this->metaCallbacks,'saveProjectHomeCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectComponentsCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectTeamsCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectResultsCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectDidacticActivitiesCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectPublicationsCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectMiscellanousCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectContactCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectStartDateCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectEndDateCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectTypeCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectNewsCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectExternalLinkCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectRestrictedLinkCallback'));
        add_action('save_post',array($this->metaCallbacks,'saveProjectServicesCallback'));
    }

    public function addProjectMetaboxes()
    {

        add_meta_box(
            'project-home',
            'Home*',
            array($this->metaCallbacks,'addProjectHomeCallback'),
            'nimp-projects',
            'normal',
            'high'

        );

        add_meta_box(
            'project-components',
            'Project Components',
            array($this->metaCallbacks,'addProjectComponentsCallback'),
            'nimp-projects',
            'normal',
            'high'

        );
        add_meta_box(
            'project-teams',
            'Teams',
            array($this->metaCallbacks,'addProjectTeamsCallback'),
            'nimp-projects',
            'normal',
            'high'

        );
        add_meta_box(
            'project-news',
            'News',
            array($this->metaCallbacks,'addProjectNewsCallback'),
            'nimp-projects',
            'normal',
            'high'

        );

        add_meta_box(
            'project-results',
            'Results',
            array($this->metaCallbacks,'addProjectResultsCallback'),
            'nimp-projects',
            'normal',
            'high'

        );
        add_meta_box(
            'project-didactic-activities',
            'Didactic Activities',
            array($this->metaCallbacks,'addProjectDidacticActivitiesCallback'),
            'nimp-projects',
            'normal',
            'high'

        );
        add_meta_box(
            'project-publications',
            'Publications',
            array($this->metaCallbacks,'addProjectPublicationsCallback'),
            'nimp-projects',
            'normal',
            'high'

        );

        add_meta_box(
            'project-services',
            'Project services ',
            array($this->metaCallbacks,'addProjectServicesCallback'),
            'nimp-projects',
            'normal',
            'high'

        );
        add_meta_box(
            'project-miscellanous',
            'Miscellanous',
            array($this->metaCallbacks,'addProjectMiscellanousCallback'),
            'nimp-projects',
            'normal',
            'high'

        );

        add_meta_box(
            'project-contact',
            'Contact',
            array($this->metaCallbacks,'addProjectContactCallback'),
            'nimp-projects',
            'normal',
            'high'

        );
        add_meta_box(
            'project-start-date',
            'Start Date',
            array($this->metaCallbacks,'addProjectStartDateCallback'),
            'nimp-projects',
            'side',
            'low'

        );
        add_meta_box(
            'project-end-date',
            'End Date',
            array($this->metaCallbacks,'addProjectEndDateCallback'),
            'nimp-projects',
            'side',
            'low'

        );
        add_meta_box(
            'project-type',
            'Project Type',
            array($this->metaCallbacks,'addProjectTypeCallback'),
            'nimp-projects',
            'side',
            'low'

        );

        add_meta_box(
            'project-external-link',
            'Project External Link ',
            array($this->metaCallbacks,'addProjectExternalLinkCallback'),
            'nimp-projects',
            'side',
            'low'

        );
        add_meta_box(
            'project-restricted-link',
            'Project Restricted Area Link ',
            array($this->metaCallbacks,'addProjectRestrictedLinkCallback'),
            'nimp-projects',
            'side',
            'low'

        );

    }


}
<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 16:31
 */
namespace Base;


class BaseController{

    public $pluginPath;
    public $pluginURL;
    public $pluginBaseName;

    public function __construct(){
        $this->pluginPath=plugin_dir_path(dirname(__FILE__,2));
        $this->pluginURL=plugin_dir_url(dirname(__FILE__,2));
        $this->pluginBaseName=plugin_basename(dirname(__FILE__,3)).'/nimp-projects.php';
    }

}



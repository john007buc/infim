<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 08.11.2018
 * Time: 17:14
 */
namespace Base;

interface IService
{
    public function register();
}
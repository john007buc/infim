<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 05.11.2018
 * Time: 14:00
 * @package InfimProjects
 */

/*
Plugin Name: NIMP Projects
Plugin URI: https://akismet.com/j
Description:  This plugin allows users to add projects
Version: 1.0.0
Author: Ivan Ion
Author URI: https://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: akismet
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2018 Ion Ivan
*/


defined('ABSPATH') or exit('muiengat');


if(file_exists($autoload_file=dirname(__FILE__).'/vendor/autoload.php')){
    require_once $autoload_file;
}



function activateNimpProjects(){
    \Actions\Activate::activate();
}
register_activation_hook(__FILE__,'activateNimpProjects');

function deactivateNimpProjects(){
    \Actions\Deactivate::deactivate();
}
register_deactivation_hook(__FILE__,'deactivateNimpProjects');


\Actions\Init::registerServices();



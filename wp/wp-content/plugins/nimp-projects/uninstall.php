<?php
/**
 * Created by PhpStorm.
 * User: ywa
 * Date: 05.11.2018
 * Time: 18:13
 * @package InfimProjects
 */

if(!defined('WP_UNINSTALL_PLUGIN')){
    exit('muiengat');
}

global $wpdb;

$wpdb->query("DELETE FROM wp_posts WHERE post_type='project'");

$wpdb->query("DELETE FROM wp_postmeta WHERE post_id NOT IN (SELECT id FROM wp_posts)");

$wpdb->query("DELETE FROM wp_term_relationship WHERE object_id NOT IN (SELECT id FROM wp_posts)");


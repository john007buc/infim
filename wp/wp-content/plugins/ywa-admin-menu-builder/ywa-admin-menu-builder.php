<?php
/**
 * @package Ywa Admin menu Builder
 */

/*
Plugin Name: Ywa Admin Menu Builder
Plugin URI: https://www.infim.ro
Description: Create custom posts
Version: 1.0.0
Author: Ion Ivan
Author URI: https://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: ywa-admin-menu-builder
Domain Path: /languages
*/

//Include custom-functions.php

require_once 'custom-functions.php';


?>
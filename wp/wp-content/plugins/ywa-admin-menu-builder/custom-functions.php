<?php

//Defins constants

define('IHS_ADMIN_MENU_URI',plugins_url('ywa-admin-menu-builder'));

define('IHS_ADMIN_MENU_JS_URI',plugins_url('ywa-admin-menu-builder').'/vendor/js');

define('IHS_ADMIN_MENU_CSS_URI',plugins_url('ywa-admin-menu-builder').'/css');
define('IHS_ADMIN_MENU_ADMIN_CSS_URI',plugins_url('ywa-admin-menu-builder').'/admin/css');
define('IHS_ADMIN_MENU_ADMIN_JS_URI',plugins_url('ywa-admin-menu-builder').'/admin/js');
if(!function_exists('ywa_enqueue_scripts')){

    function ywa_enqueue_scripts(){
       wp_enqueue_style('ywa_menu_style',IHS_ADMIN_MENU_CSS_URI.'/style.css');
       wp_enqueue_script('ywa_menu_main_js',IHS_ADMIN_MENU_JS_URI.'/main.js',array('jquery'),'',true);
    }
}
add_action('wp_enqueue_scripts', 'ywa_enqueue_scripts');

if(!function_exists('ywa_enqueue_admin_scripts')){

   
    function ywa_enqueue_admin_scripts($hook){
       //wp_die($hook);
        if('edit.php'!=$hook){
            return;
        }
       wp_enqueue_style('ywa_admin_menu_style',IHS_ADMIN_MENU_ADMIN_CSS_URI.'/admin.css');
       wp_enqueue_script('ywa_admin_menu_js',IHS_ADMIN_MENU_ADMIN_JS_URI.'/admin.js',array('jquery'),'',true);
    }
}
add_action('admin_enqueue_scripts', 'ywa_enqueue_admin_scripts')




?>
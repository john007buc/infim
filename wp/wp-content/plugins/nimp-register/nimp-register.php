<?php
/*
Plugin Name: NIMP User Register
Plugin URI:
Description: User management functionality for the portfolio
management application.
Author: Rakhitha Nimesh
Version: 1.0
Author URI: http://www.innovativephp.com/
*/

class NIMP_Register{
    public function __construct() {

        add_action('init', array( $this,'manage_user_routes' ) );

        add_filter( 'query_vars', array( $this,
            'manage_user_routes_query_vars' ) );

        register_activation_hook( __FILE__, array( $this,
            'flush_application_rewrite_rules' ) );

        add_action( 'template_redirect', array( $this, 'front_controller'
        ) );

        add_action( 'wpwa_register_user', array( $this, 'register_user' )
        );

    }



    public function manage_user_routes() {
        add_rewrite_rule( '^user/([^/]+)/?',
            'index.php?control_action=$matches[1]', 'top' );
    }

    public function manage_user_routes_query_vars( $query_vars ) {
        $query_vars[] = 'control_action';
        return $query_vars;
    }

    public function flush_application_rewrite_rules() {
        $this->manage_user_routes();
        flush_rewrite_rules();
    }

    public function front_controller() {
        global $wp_query;
        $control_action = isset ( $wp_query->query_vars['control_action'] ) ? $wp_query->query_vars['control_action'] : '';


       switch ( $control_action ) {
            case 'registernimpbrgtynty56575':
               do_action( 'wpwa_register_user' );
                break;
          }
    }

    public function register_user() {

        if ( $_POST ) {
            $errors = array();
            $user_login = ( (isset ( $_POST['wpwa_user'] ) && strlen($_POST['wpwa_user'])<100) ? $_POST['wpwa_user'] : '' );

            $user_email = ( (isset ( $_POST['wpwa_email'] )&& strlen($_POST['wpwa_email'])<100 ) ?$_POST['wpwa_email'] : '' );

            $last_name=( (isset ( $_POST['wpwa_last_name'])&& strlen($_POST['wpwa_last_name'])<100) ?$_POST['wpwa_last_name'] : '' );
            $first_name=( (isset ( $_POST['wpwa_first_name'])&& strlen($_POST['wpwa_first_name'])<100) ?$_POST['wpwa_first_name'] : '' );
            $password1 = ( (isset ( $_POST['password1']) && strlen($_POST['password1'])<100) ?$_POST['password1'] : '' );
            $password2 = ( (isset ( $_POST['password2']) && strlen($_POST['password2'])<100)?$_POST['password2'] : '' );

            $user_type = 'nimp-user';

            if ( empty( $user_login ) )
                array_push($errors, __('Please enter a username.','wpwa') );
            if ( empty( $user_email ) )
                array_push( $errors, __('Please enter e-mail.','wpwa') );
            if ( empty( $user_type ) )
                array_push( $errors, __('Please enter user type.','wpwa') );

            if ( empty( $password1 ) || $password1!=$password2)
                array_push( $errors, __('Please enter passwords again','wpwa') );

            if ( empty( $first_name ) )
                array_push( $errors, __('Please enter  first name again.','wpwa') );

            if ( empty( $last_name ) )
                array_push( $errors, __('Please enter  last name again.','wpwa') );



            $sanitized_user_login = sanitize_user( $user_login );
            $sanitized_first_name=sanitize_user($first_name);
            $sanitized_last_name=sanitize_user($last_name);
            if ( !empty($user_email) && !is_email( $user_email ) )
                array_push( $errors, __('Please enter valid email.','wpwa'));
            elseif ( email_exists( $user_email ) )
                array_push( $errors, __('User with this email already registered.','wpwa'));

            if ( empty( $sanitized_user_login ) || !validate_username( $user_login ) )

                array_push( $errors, __('Invalid username.','wpwa') );
            elseif ( username_exists( $sanitized_user_login ) )
                array_push( $errors, __('Username already exists.','wpwa') );


            if ( empty( $errors ) ) {
                //$user_pass = wp_generate_password();

                $user_id= wp_insert_user( array('user_login' =>$sanitized_user_login,
                        'first_name'=>$sanitized_first_name,
                        'last_name'=>$sanitized_last_name,
                        'user_email' => $user_email,
                        'role' => $user_type,
                        'user_pass' => $password1)
                );

                if ( !$user_id ) {
                    array_push( $errors, __('Registration failed.','wpwa') );
                }else{
                   // wp_new_user_notification( $user_id, null,'user');
                    $success_message = __('Registration completed successfully.Please click to  LOGIN btton from the navigation bar.','wpwa');
                 }
                }
            }




        //including the template

        if ( !is_user_logged_in() ) {
            include dirname(__FILE__) . '/templates/register-template.php';
            exit;
        }

    }


}
$nimpRegister = new NIMP_Register();



<?php  get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-lg-3 col-md-3">
        </div>
        <div class="col-sm-12 col-lg-6 col-md-6">

            <?php
               if( isset($errors) && count( $errors ) > 0) {
                 foreach( $errors as $error ){
                     echo "<div class='alert alert-danger' role='alert'>{$error}</div>";
                 }
              }
            if(isset($success_message )){
                echo "<div class='alert alert-success' role='alert'>{$success_message}</div>";
            }
            ?>

            <form id='registration-form' method='post' action='<?php echo
                get_site_url() . '/user/registernimpbrgtynty56575'; ?>'>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username (Ex: alina.ionescu)</label>
                    <input type="text" class="form-control" name='wpwa_user' aria-describedby="userHelp" placeholder="Enter username" value='<?php echo isset(
                        $user_login ) ? $user_login : ''; ?>' >

                </div>
                <div class="form-group">
                    <label >First Name</label>
                    <input type="text" class="form-control" name='wpwa_first_name'  placeholder="Enter first name" value='<?php echo isset(
                        $first_name ) ? $first_name : ''; ?>' >
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" name='wpwa_last_name'  placeholder="Enter last name" value='<?php echo isset(
                        $last_name ) ? $last_name : ''; ?>' >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" name='wpwa_email'  aria-describedby="emailHelp" placeholder="Enter email" value='<?php echo isset(
                        $user_email ) ? $user_email : ''; ?>'>
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password </label>
                    <input type="password" class="form-control" name="password1" id="exampleInputPassword1" placeholder="Enter password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password again</label>
                    <input type="password" class="form-control" name="password2" id="exampleInputPasswords" placeholder="Retype password">
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>


        </div>
        <div class="col-sm-12 col-lg-3 col-md-3">
        </div>
    </div>
</div>
<?php get_footer(); ?>
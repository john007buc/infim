( function ( $ ) {
    $( document ).ready( function () {

        //Require post title when adding/editing Project Summaries
        $( "form" ).submit(function(){
            var testervar = jQuery('[id^="titlediv"]')
                .find('#title');
            if (testervar.val().length < 1)
            {
                jQuery('[id^="title"]').css('background', '#F96');
                setTimeout("jQuery('#ajax-loading').css('visibility', 'hidden');", 100);
                alert('POST TITLE is required');
                setTimeout("jQuery('#publish').removeClass('button-primary-disabled');", 100);
                return false;
            }
        });
        $( 'body .input[type=submit]' ).on( 'click', function () {

            // If the title isn't set
            if ( $( "#title" ).val().replace( / /g, '' ).length === 0 ) {

                // Show the alert
                window.alert( 'A title is required.' );

                // Hide the spinner
                $( '#major-publishing-actions .spinner' ).hide();

                // The buttons get "disabled" added to them on submit. Remove that class.
                $( '#major-publishing-actions' ).find( ':button, :submit, a.submitdelete, #post-preview' ).removeClass( 'disabled' );

                // Focus on the title field.
                $( "#title" ).focus();

                return false;
            }
        });
    });
}( jQuery ) );
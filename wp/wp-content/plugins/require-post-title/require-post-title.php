<?php
/*
Plugin Name: Require Post Title
Plugin URI: http://infim.ro
Description: Forces user to assign a Title to a post before publishing
Author: Ion Ivan
Version: 0.1

*/


function load_admin_scripts( $hook ) {
    wp_enqueue_script( 'admin_scripts', plugins_url('/assets/js/force-title.js',__FILE__), array( 'jquery' ) );
}

add_action( 'admin_enqueue_scripts', 'load_admin_scripts' );

